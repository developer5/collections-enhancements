/*
 * The MIT License
 *
 * Copyright 2016 Justin Ahrens.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.drjustongray.counters;

import java.util.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Justin Ahrens
 */
public class CharacterCounterTest {

    Random random;

    @Before
    public void setUp() {
	random = new Random();
    }

    @After
    public void tearDown() {
	random = null;
    }

    /**
     * Test of countOf method, of class CharacterCounter.
     */
    @Test
    public void testCountOf() {
	System.out.println("countOf");
	int[] counts = new int[26];
	StringBuilder sb = new StringBuilder();
	for (int i = 0; i < 1000; i++) {
	    int j = random.nextInt(26);
	    counts[j]++;
	    sb.append((char) (j + 97));
	}

	CharacterCounter counter = new CharacterCounter(sb);
	for (int i = 0; i < 26; i++) {
	    assertEquals(counts[i], counter.countOf((char) (i + 97)));
	}
    }

    /**
     * Test of keySet method, of class CharacterCounter.
     */
    @Test
    public void testKeySet() {
	System.out.println("keySet");
	HashSet<Character> keys = new HashSet<>();
	StringBuilder sb = new StringBuilder();
	for (int i = 0; i < 20; i++) {
	    int j = random.nextInt(26);
	    Character c = (char) (j + 97);
	    keys.add(c);
	    sb.append(c);
	}

	CharacterCounter counter = new CharacterCounter(sb);
	assertEquals(keys, counter.keySet());
    }

}

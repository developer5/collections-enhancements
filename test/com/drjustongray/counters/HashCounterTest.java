/*
 * The MIT License
 *
 * Copyright 2016 Justin Ahrens.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.drjustongray.counters;

import java.util.*;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Justin Ahrens
 */
public class HashCounterTest {
    Random random;
    
    public HashCounterTest() {
    
    }
    @Before
    public void setUp() {
	random = new Random();
    }
    
    @After
    public void tearDown() {
	random = null;
    }

    /**
     * Test of countOf method, of class HashCounter.
     */
    @Test
    public void testCountOf() {
	System.out.println("countOf");
	int[] counts = new int[100];
	HashCounter instance = new HashCounter();
	for (int i = 0; i < 10000; i++) {
	    int j = random.nextInt(100);
	    counts[j]++;
	    instance.increment(j);
	}
	for (int i = 0; i < 10000; i++) {
	    int j = random.nextInt(100);
	    counts[j]--;
	    instance.decrement(j);
	}
	for (int i = 0; i < 100; i++) {
	    assertEquals(counts[i], instance.countOf(i));
	}
    }

    /**
     * Test of increment method, of class HashCounter.
     */
    @Test
    public void testIncrement() {
	System.out.println("increment");
	int[] counts = new int[100];
	HashCounter instance = new HashCounter();
	for (int i = 0; i < 10000; i++) {
	    int j = random.nextInt(100);
	    assertEquals(++counts[j], instance.increment(j));
	}
    }

    /**
     * Test of decrement method, of class HashCounter.
     */
    @Test
    public void testDecrement() {
	System.out.println("decrement");
	int[] counts = new int[100];
	HashCounter instance = new HashCounter();
	for (int i = 0; i < 10000; i++) {
	    int j = random.nextInt(100);
	    counts[j]++;
	    instance.increment(j);
	}
	for (int i = 0; i < 10000; i++) {
	    int j = random.nextInt(100);
	    assertEquals(--counts[j], instance.decrement(j));
	}
    }

    /**
     * Test of set method, of class HashCounter.
     */
    @Test
    public void testSet() {
	System.out.println("set");
	int[] counts = new int[100];
	HashCounter instance = new HashCounter();
	for (int i = 0; i < 10000; i++) {
	    int j = random.nextInt(100);
	    counts[j]++;
	    instance.increment(j);
	}
	for (int i = 0; i < 100; i++) {
	    int j = random.nextInt();
	    assertEquals(counts[i], instance.set(i, j));
	    assertEquals(j, instance.countOf(i));
	}
    }

    /**
     * Test of clear method, of class HashCounter.
     */
    @Test
    public void testClear() {
	System.out.println("clear");
	int[] counts = new int[100];
	HashCounter instance = new HashCounter();
	for (int i = 0; i < 10000; i++) {
	    int j = random.nextInt(100);
	    counts[j]++;
	    instance.increment(j);
	}
	for (int i = 0; i < 100; i++) {
	    assertEquals(counts[i], instance.clear(i));
	    assertEquals(0, instance.countOf(i));
	}
    }

    /**
     * Test of keySet method, of class HashCounter.
     */
    @Test
    public void testKeySet() {
	System.out.println("keySet");
	HashSet keys = new HashSet();
	HashCounter instance = new HashCounter();
	for (int i = 0; i < 10000; i++) {
	    int j = random.nextInt(100);
	    keys.add(j);
	    instance.increment(j);
	}
	assertEquals(keys, instance.keySet());
    }
    
}

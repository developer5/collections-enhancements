/*
 * The MIT License
 *
 * Copyright 2016 Justin Ahrens.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.drjustongray.referencevaluemap;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Justin Ahrens
 */
public class ReferenceValueMapTest {
    
    Random random;
    
    public ReferenceValueMapTest() {
    }
    
    @Before
    public void setUp() {
	random = new Random();
    }
    
    @After
    public void tearDown() {
	random = null;
    }

    /**
     * Test of generateReference method, of class WeakValueMap.
     */
    @Test
    public void testGenerateReference() {
	System.out.println("generateReference");
	WeakValueMap instance = new WeakValueMap();
	Object value = new Object();
	Reference result = instance.generateReference(value);
	assertEquals(value, result.get());
    }

    /**
     * Test of size method, of class ReferenceValueMap.
     */
    @Test
    public void testSize() {
	System.out.println("size");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	assertEquals(0, instance.size());
	for (int i = 0; i < 100;) {
	    instance.put(i, Long.toHexString(random.nextLong()));
	    assertEquals(++i, instance.size());
	}
    }

    /**
     * Test of isEmpty method, of class ReferenceValueMap.
     */
    @Test
    public void testIsEmpty() {
	System.out.println("isEmpty");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	assertTrue(instance.isEmpty());
	for (int i = 0; i < 100; i++) {
	    instance.put(i, Long.toHexString(random.nextLong()));
	    assertFalse(instance.isEmpty());
	}
    }

    /**
     * Test of containsKey method, of class ReferenceValueMap.
     */
    @Test
    public void testContainsKey() {
	System.out.println("containsKey");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	for (int i = 0; i < 100; i++) {
	    assertFalse(instance.containsKey(i));
	    instance.put(i, Long.toHexString(random.nextLong()));
	    assertTrue(instance.containsKey(i));
	}
    }

    /**
     * Test of containsValue method, of class ReferenceValueMap.
     */
    @Test
    public void testContainsValue() {
	System.out.println("containsValue");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	for (int i = 0; i < 100; i++) {
	    String s = Long.toHexString(random.nextLong());
	    instance.put(i, s);
	    assertTrue(instance.containsValue(s));
	    assertFalse(instance.containsValue(i));
	}
    }

    /**
     * Test of get method, of class ReferenceValueMap.
     */
    @Test
    public void testGet() {
	System.out.println("get");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	for (int i = 0; i < 100; i++) {
	    String s = Long.toHexString(random.nextLong());
	    instance.put(i, s);
	    assertEquals(s, instance.get(i));
	}
    }

    /**
     * Test of put method, of class ReferenceValueMap.
     */
    @Test
    public void testPut() {
	System.out.println("put");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	for (int i = 0; i < 100; i++) {
	    String s = Long.toHexString(random.nextLong());
	    assertEquals(null, instance.put(i, s));
	    assertEquals(s, instance.put(i, i));
	}
    }

    /**
     * Test of remove method, of class ReferenceValueMap.
     */
    @Test
    public void testRemove() {
	System.out.println("remove");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	for (int i = 0; i < 100; i++) {
	    String s = Long.toHexString(random.nextLong());
	    assertEquals(null, instance.remove(i));
	    instance.put(i, s);
	    assertEquals(s, instance.remove(i));
	    assertFalse(instance.containsKey(i));
	}
    }

    /**
     * Test of putAll method, of class ReferenceValueMap.
     */
    @Test
    public void testPutAll() {
	System.out.println("putAll");
	HashMap map = new HashMap();
	for (int i = 0; i < 100; i++) {
	    map.put(i, Long.toHexString(random.nextLong()));
	}
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	instance.putAll(map);
	for (int i = 0; i < 100; i++) {
	    assertEquals(map.get(i), instance.get(i));
	}
    }

    /**
     * Test of clear method, of class ReferenceValueMap.
     */
    @Test
    public void testClear() {
	System.out.println("clear");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	for (int i = 0; i < 100; i++) {
	    instance.put(i, Long.toHexString(random.nextLong()));
	}
	instance.clear();
	assertTrue(instance.isEmpty());
    }

    /**
     * Test of entrySet method, of class ReferenceValueMap.
     */
    @Test
    public void testEntrySet() {
	System.out.println("entrySet");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	HashMap map = new HashMap();
	for (int i = 0; i < 100; i++) {
	    int key = random.nextInt();
	    String value = Long.toHexString(random.nextLong());
	    instance.put(key, value);
	    map.put(key, value);
	}
	assertEquals(map.entrySet(), instance.entrySet());
    }

    /**
     * Test of keySet method, of class ReferenceValueMap.
     */
    @Test
    public void testKeySet() {
	System.out.println("keySet");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	HashMap map = new HashMap();
	for (int i = 0; i < 100; i++) {
	    int key = random.nextInt();
	    String value = Long.toHexString(random.nextLong());
	    instance.put(key, value);
	    map.put(key, value);
	}
	assertEquals(map.keySet(), instance.keySet());
    }

    /**
     * Test of values method, of class ReferenceValueMap.
     */
    @Test
    public void testValues() {
	System.out.println("values");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	HashMap map = new HashMap();
	for (int i = 0; i < 100; i++) {
	    int key = random.nextInt();
	    String value = Long.toHexString(random.nextLong());
	    map.put(key, value);
	    instance.put(key, value);
	}
	assertTrue(map.values().containsAll(instance.values()));
	assertEquals(map.values().size(), instance.values().size());
    }

    /**
     * Test of getReferenceValue method, of class ReferenceValueMap.
     */
    @Test
    public void testGetReferenceValue() {
	System.out.println("getReferenceValue");
	Object o = new Object();
	Reference reference = new WeakReference(o);
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	assertEquals(o, instance.getReferenceValue(reference));
	assertEquals(null, instance.getReferenceValue(null));
    }

    /**
     * Test of processRemovalQueue method, of class ReferenceValueMap.
     */
    @Test
    public void testProcessRemovalQueue() {
	System.out.println("processRemovalQueue");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	HashMap map = new HashMap();
	
	for (int i = 0; i < 100; i++) {
	    int key = random.nextInt();
	    String value = Long.toHexString(random.nextLong());
	    instance.put(key, value);
	    map.put(key, value);
	}
	for (int i = 0; i < 100; i += 2) {
	    map.remove(i);
	}
	
	Reference ref = new WeakReference(new Object());
	while (ref.get() != null) {
	    System.gc();
	}
	instance.processRemovalQueue();
	
	assertEquals(map, instance);
    }

    /**
     * Test of equals method, of class ReferenceValueMap.
     */
    @Test
    public void testEquals() {
	System.out.println("equals");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	HashMap map = new HashMap();
	
	for (int i = 0; i < 100; i++) {
	    int key = random.nextInt();
	    String value = Long.toHexString(random.nextLong());
	    instance.put(key, value);
	    map.put(key, value);
	}
	
	assertTrue(map.equals(instance));
    }

    /**
     * Test of hashCode method, of class ReferenceValueMap.
     */
    @Test
    public void testHashCode() {
	System.out.println("hashCode");
	ReferenceValueMap instance = new ReferenceValueMapImpl();
	HashMap map = new HashMap();
	
	for (int i = 0; i < 100; i++) {
	    int key = random.nextInt();
	    String value = Long.toHexString(random.nextLong());
	    instance.put(key, value);
	    map.put(key, value);
	}
	
	assertEquals(map.hashCode(), instance.hashCode());
    }

    public class ReferenceValueMapImpl<K, V> extends ReferenceValueMap<K, V> {

	@Override
	Reference<V> generateReference(V value) {
	    return new WeakReference<>(value);
	}
    }
    
}

/*
The MIT License (MIT)

Copyright (c) 2016 Justin Ahrens (developer@drjustongray.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.drjustongray.referencevaluemap;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.*;

/**
 * An abstract map that holds values as references in an arbitrary, supplied
 * internal map. An internal {@link ReferenceQueue} is kept to remove references
 * whose values have been garbage-collected. The queue is processed during all
 * calls to methods that produce structural changes, as well as when new
 * iterators are being created. There is an option to have the queue processed
 * during all {@link Map} calls, but it should be used with care. Subclasses
 * determine which type of reference to use.
 *
 * @author Justin Ahrens
 * @version 1.0
 */
public abstract class ReferenceValueMap<K, V> implements Map<K, V> {

    final ReferenceQueue<V> referenceQueue = new ReferenceQueue<>();
    private final Map<K, Reference<V>> internalMap;
    private Set<Map.Entry<K, V>> entrySet;
    private Set<K> keySet;
    private Collection<V> values;
    private boolean alwaysProcessQueue = false;

    /**
     * Constructs an instance which uses a {@link HashMap} internally.
     */
    public ReferenceValueMap() {
	this.internalMap = new HashMap<>();
    }

    /**
     * @param map The map the {@link ReferenceValueMap} will use internally. It
     * must be non-null and empty, and there should be no external references to
     * it kept.
     */
    ReferenceValueMap(Map<K, Reference<V>> map) {
	if (map == null || !map.isEmpty()) {
	    throw new IllegalArgumentException("Internal map must be non-null and empty");
	}
	if (!map.isEmpty()) {
	    throw new IllegalArgumentException("Internal Map Must Be Empty");
	}
	internalMap = map;
    }

    /**
     * Call to set whether the internal {@link ReferenceQueue} is processed
     * during every map call, or just those that produce structural changes.
     * Processing the {@link ReferenceQueue} while iterating over the entry set,
     * the key set, or values collection can cause a
     * {@link ConcurrentModificationException}, but if you know that this will
     * not happen, setting this value to true might be beneficial.
     *
     * @param alwaysProcessQueue If true, all map calls will cause the queue to
     * be processed. The default is false.
     */
    public void setAlwaysProcessQueue(boolean alwaysProcessQueue) {
	this.alwaysProcessQueue = alwaysProcessQueue;
    }

    @Override
    public int size() {
	if (alwaysProcessQueue) {
	    processRemovalQueue();
	}
	return internalMap.size();
    }

    @Override
    public boolean isEmpty() {
	return size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
	if (alwaysProcessQueue) {
	    processRemovalQueue();
	}
	return internalMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
	if (alwaysProcessQueue) {
	    processRemovalQueue();
	}
	return values().contains(value);
    }

    @Override
    public V get(Object key) {
	if (alwaysProcessQueue) {
	    processRemovalQueue();
	}
	return getReferenceValue(internalMap.get(key));
    }

    @Override
    public V put(K key, V value) {
	processRemovalQueue();
	return getReferenceValue(internalMap.put(key, generateReference(value)));
    }

    @Override
    public V remove(Object key) {
	processRemovalQueue();
	return getReferenceValue(internalMap.remove(key));
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
	if (m != null) {
	    for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
		put(entry.getKey(), entry.getValue());
	    }
	}
    }

    @Override
    public void clear() {
	internalMap.clear();
	processRemovalQueue();
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
	if (entrySet == null) {
	    entrySet = new EntrySet();
	}
	return entrySet;
    }

    @Override
    public Set<K> keySet() {
	if (keySet == null) {
	    keySet = new KeySet();
	}
	return keySet;
    }

    @Override
    public Collection<V> values() {
	if (values == null) {
	    values = new Values();
	}
	return values;
    }

    @Override
    public boolean equals(Object other) {
	if (other == this) {
	    return true;
	} else if (other instanceof Map) {
	    return entrySet().equals(((Map) other).entrySet());
	} else {
	    return false;
	}
    }

    @Override
    public int hashCode() {
	return Objects.hashCode(entrySet());
    }

    abstract Reference<V> generateReference(V value);

    V getReferenceValue(Reference<? extends V> reference) {
	return reference == null ? null : reference.get();
    }

    void processRemovalQueue() {
	Reference referenceToRemove;
	while ((referenceToRemove = referenceQueue.poll()) != null) {
	    internalMap.values().remove(referenceToRemove);
	}
    }

    private class EntrySet extends AbstractSet<Map.Entry<K, V>> {

	@Override
	public void clear() {
	    ReferenceValueMap.this.clear();
	}

	@Override
	public Iterator<Map.Entry<K, V>> iterator() {
	    processRemovalQueue();
	    return new Iterator<Map.Entry<K, V>>() {
		Iterator<Map.Entry<K, Reference<V>>> internalIterator
			= internalMap.entrySet().iterator();

		@Override
		public boolean hasNext() {
		    return internalIterator.hasNext();
		}

		@Override
		public Map.Entry<K, V> next() {
		    return new Entry(internalIterator.next());
		}

		@Override
		public void remove() {
		    internalIterator.remove();
		}
	    };
	}

	@Override
	public int size() {
	    return ReferenceValueMap.this.size();
	}
    }

    private class KeySet extends AbstractSet<K> {

	Set<K> internalKeySet = internalMap.keySet();

	@Override
	public void clear() {
	    ReferenceValueMap.this.clear();
	}

	@Override
	public boolean contains(Object object) {
	    processRemovalQueue();
	    return internalKeySet.contains(object);
	}

	@Override
	public Iterator<K> iterator() {
	    processRemovalQueue();
	    return internalKeySet.iterator();
	}

	@Override
	public boolean remove(Object object) {
	    processRemovalQueue();
	    return internalKeySet.remove(object);
	}

	@Override
	public int size() {
	    return ReferenceValueMap.this.size();
	}
    }

    private class Values extends AbstractCollection<V> {

	@Override
	public void clear() {
	    ReferenceValueMap.this.clear();
	}

	@Override
	public Iterator<V> iterator() {
	    processRemovalQueue();
	    return new Iterator<V>() {
		Iterator<Reference<V>> internalIterator = internalMap.values().iterator();

		@Override
		public boolean hasNext() {
		    return internalIterator.hasNext();
		}

		@Override
		public V next() {
		    return getReferenceValue(internalIterator.next());
		}

		@Override
		public void remove() {
		    internalIterator.remove();
		}
	    };
	}

	@Override
	public int size() {
	    return ReferenceValueMap.this.size();
	}
    }

    private class Entry implements Map.Entry<K, V> {

	private final Map.Entry<K, Reference<V>> internalEntry;

	Entry(Map.Entry<K, Reference<V>> internalEntry) {
	    super();
	    this.internalEntry = internalEntry;
	}

	@Override
	public K getKey() {
	    return internalEntry.getKey();
	}

	@Override
	public V getValue() {
	    return getReferenceValue(internalEntry.getValue());
	}

	@Override
	public V setValue(V value) {
	    return getReferenceValue(internalEntry.setValue(generateReference(value)));
	}

	@Override
	public boolean equals(Object other) {
	    if (other == this) {
		return true;
	    } else if (other instanceof Map.Entry) {
		Map.Entry entry = (Map.Entry) other;
		return (getKey() == null ? entry.getKey() == null
			: getKey().equals(entry.getKey()))
			&& (getValue() == null ? entry.getValue() == null
				: getValue().equals(entry.getValue()));
	    } else {
		return false;
	    }
	}

	@Override
	public int hashCode() {
	    return Objects.hashCode(getKey()) ^ Objects.hashCode(getValue());
	}
    }
}

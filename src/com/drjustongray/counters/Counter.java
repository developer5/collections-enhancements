/*
The MIT License (MIT)

Copyright (c) 2016 Justin Ahrens (developer@drjustongray.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.drjustongray.counters;

import java.util.Set;

/**
 * An object that keeps an integer count associated with objects of type T.
 * Every method is optional except for
 * {@link #countOf(java.lang.Object) countOf} and {@link #keySet() keySet}.
 * Allowance of negative counts is optional. Implementations should throw an
 * {@link IllegalStateException} if negative counts are disallowed and
 * {@link #decrement(java.lang.Object) decrement(Object)} is called for an
 * object whose count is 0.
 *
 * @author Justin Ahrens
 * @param <T> Type of object to count
 */
public interface Counter<T> {

    /**
     * Returns the count associated with an object. Returns 0 if there is no
     * count recorded.
     *
     * @param key The object to get the count of
     * @return The count associated with the given key
     */
    int countOf(T key);

    /**
     * Increases the count associated with the given key by 1.
     *
     * @param key
     * @return The current count associated with the key
     */
    int increment(T key);

    /**
     * Decreases the count associated with the given key by 1.
     *
     * @param key
     * @return The current count associated with the key
     */
    int decrement(T key);

    /**
     * Sets the count associated with the given key to the given value.
     *
     * @param key
     * @param count
     * @return The previous count
     */
    int set(T key, int count);

    /**
     * Removes the count associated with the given key.
     *
     * @param key
     * @return The previous count
     */
    int clear(T key);

    /**
     * Returns an immutable {@link Set} containing all keys the counter holds.
     *
     * @return The set of all keys in this counter
     */
    Set<T> keySet();
}

/*
The MIT License (MIT)

Copyright (c) 2016 Justin Ahrens (developer@drjustongray.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.drjustongray.counters;

import java.util.Set;

/**
 * An immutable implementation of {@link Counter} that counts the characters of
 * a given {@link CharSequence}. Uses a {@link HashCounter} internally.
 *
 * @author Justin Ahrens
 */
public class CharacterCounter extends AbstractCounter<Character> {

    private final HashCounter<Character> internalCounter = new HashCounter<>();

    public CharacterCounter(CharSequence s) {
	for (int i = 0; i < s.length(); i++) {
	    internalCounter.increment(s.charAt(i));
	}
    }

    @Override
    public int countOf(Character key) {
	return internalCounter.countOf(key);
    }

    @Override
    public Set<Character> keySet() {
	return internalCounter.keySet();
    }
}

/*
The MIT License (MIT)

Copyright (c) 2016 Justin Ahrens (developer@drjustongray.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.drjustongray.counters;

import java.util.*;

/**
 * An implementation of {@link Counter} that uses a {@link HashMap} internally.
 * Negative counts are permitted.
 *
 * @author Justin Ahrens
 */
public final class HashCounter<T> implements Counter<T> {

    private final HashMap<T, Integer> internalMap = new HashMap<>();
    private Set<T> keySet;

    /**
     * Constructs an empty counter.
     */
    public HashCounter() {

    }

    /**
     * Constructs an instance in which the elements of the given array are
     * counted.
     *
     * @param array
     */
    public HashCounter(T[] array) {
	for (T t : array) {
	    increment(t);
	}
    }

    /**
     * Constructs an instance in which the elements of the given collection are
     * counted.
     *
     * @param collection
     */
    public HashCounter(Collection<T> collection) {
	for (T t : collection) {
	    increment(t);
	}
    }

    @Override
    public int countOf(T key) {
	return internalMap.containsKey(key) ? internalMap.get(key) : 0;
    }

    @Override
    public int increment(T key) {
	internalMap.put(key, countOf(key) + 1);
	return countOf(key);
    }

    @Override
    public int decrement(T key) {
	internalMap.put(key, countOf(key) - 1);
	return countOf(key);
    }

    @Override
    public int set(T key, int count) {
	int oldCount = countOf(key);
	internalMap.put(key, count);
	return oldCount;
    }

    @Override
    public int clear(T key) {
	int oldCount = countOf(key);
	internalMap.remove(key);
	return oldCount;
    }

    @Override
    public Set<T> keySet() {
	if (keySet == null) {
	    keySet = Collections.unmodifiableSet(internalMap.keySet());
	}
	return keySet;
    }
}

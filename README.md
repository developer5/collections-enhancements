# Collection Enhancements

Collection Enhancements is a library which provides classes that complement and extend Java's collections framework.
At the moment, these include an interface for, and two concrete implementations of, counters, as well as maps that hold each value in a soft or weak reference transparently.

## Code Example

```java
Map<K, V> weakValueHashMap = new WeakValueMap<>(new HashMap<>());
Map<K, V> softValueHashtable = new SoftValueMap<>();
```

```java
Counter<Integer> counter = new HashCounter<>();
counter.increment(-3);
counter.increment(9);
counter.decrement(8);
System.out.println("The count of -3 is " + counter.countOf(-3));
```

```java
CharacterCounter counter = new CharacterCounter("Let's count the characters in this string!");
for (Character c: counter.keySet()) {
    System.out.println(c + " occurs " + counter.countOf(c) + " times");
}
```

## Motivation

The reference value maps are intended to be used to easily create canonical maps of objects.
The counters are used to count things.

## API Reference

Javadoc comments are written into the source code. Just generate and view the Javadocs as you would with any other project.

## Tests

Unit tests are included in the test packages folder.

## License

    The MIT License (MIT)

    Copyright (c) 2016 Justin Ahrens (developer@drjustongray.com)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.